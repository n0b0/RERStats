This scripts plots the punctality of regional and suburban trains in Paris, 
Using data from the "SNCF open data" portal https://ressources.data.sncf.com/explore/dataset/ponctualite-mensuelle-transilien/information/
As I don't live anymore in/near paris it's unlikely than I do any more work
on this project. 
I used this project to see what can be done with "official" open data, and the
python code uses .csv files provided by the SNCF. A nice feature would be to 
use the JSON api (may-be for a next project)
If you ever want to revive this project, beware sometimes the SNCF just 
changes the columns name/order and can do it without any notice. 

If you really want to know the licence the project is under WTFPL

DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
                    Version 2, December 2004 

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 

 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHAT THE FUCK YOU WANT TO.