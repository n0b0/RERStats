#This code is under the WTFPL http://www.wtfpl.net/about/
# TL/DR Do what the fuck you want to do with this piece of code

#This is a toy project the code comes as-it, it was thought to be used with ipython notebook
#but should be runable stand-alone

#Preparation
import pandas as pd
from pandas.tools.plotting import scatter_matrix
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from datetime import datetime
get_ipython().magic(u'matplotlib inline')
#Color set for plots
tableau20 = [(114, 158, 206), (205, 204, 93), (103, 191, 92), (237, 102, 93),
             (173, 139, 201), (168, 120, 110), (237, 151, 202), (162, 162, 162),  
             (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),  
             (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),  
             (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]  
  
# Scale the RGB values to the [0, 1] range, which is the format matplotlib accepts.  
for i in range(len(tableau20)):  
  r, g, b = tableau20[i]  
  tableau20[i] = (r / 255., g / 255., b / 255.)  
rc('text', usetex = True)
rc("figure", facecolor="white")
rc('font', family='serif', size=14)
indir = "/path/to/data/"
outdir ="/path/to/plot/"
#read the data
dftime = pd.read_csv(indir + 'ponctualite-mensuelle-transilien.csv',sep=';')
dftime = dftime[["Date","Ligne","Taux de ponctualité"]]
dftime["Date"] = pd.tseries.tools.to_datetime(dftime["Date"])
dftime=dftime.sort(columns="Date", ascending=True)
dftime

# In[2]:
#compute the average punctuality
df2=pd.DataFrame()
Punctual = {}
for i in ["A", "B", "C", "D", "E","H","J","K","L","N","P","R","U"]:
  df2[i]=dftime.loc[dftime["Ligne"] == i].values[:, 2]
  print("Ligne : " + i +" " + str(np.mean(df2[i])))
  Punctual[i]=np.mean(df2[i])


# In[3]:
#plot for the RER, some stuffs are tune by hand (position of tests)
fig = plt.figure(figsize=(15,15))
ax = fig.gca()

colors ={"A":"ro-","B":"bx-","C":"y.-","D":"g.-","E":"m2-","N":"kv-"
for i in ["A","B","C","D","E"]:
  #print(dftime.loc[dftime["Ligne"]==i].values[:,0])
  plt.plot_date(dftime.loc[dftime["Ligne"]==i].values[:,0],dftime.loc[dftime["Ligne"]==i].values[:,2], colors[i], label = "RER "+ i)
  y = dftime.loc[dftime["Ligne"]==i].values[-1,2]
  if (i=="B"): y = y-0.5 #Offet to avoir writing the stats for B over the one for D
  plt.text("12/05/2015", y, "Ligne "+i + " " + str(round(Punctual[i]))[0:2] + "\%", color=(colors[i])[0])

plt.title(r"Ponctualit\'{e} du RER (SNCF)",fontsize=20)
plt.ylabel(r"proportion de voyageurs ``\textit{\'{a} l'heure}'' (\%)",fontsize=18)
plt.xlabel("Date",fontsize=18)
plt.text("01/05/2013",62,r"La ponctualit\'{e} moyenne 2012-2015 est donn\'{e}e \`{a} c\^{o}t\'{e} du nom de la ligne", fontsize=13,fontstyle='italic')
#This sentence is required by the SNCF by the SNCF open data licence
plt.text("01/05/2013", 61.0, r"Donn\'{e}es t\'{e}l\'{e}charg\'{e}es en f\'{e}vrier 2016 sur \textit{SNCF open-data} https://data.sncf.com/.",fontsize=12)
#Get nice axis
plt.ylim(60,100)
plt.xlim("12/01/2012","03/03/2016")
plt.grid()
ax.spines["top"].set_visible(False)  
ax.spines["bottom"].set_visible(False)  
ax.spines["right"].set_visible(False)  
ax.spines["left"].set_visible(False)  
# Ensure that the axis ticks only show up on the bottom and left of the plot.  
# Ticks on the right and top of the plot are generally unnecessary chartjunk.  
ax.get_xaxis().tick_bottom()  
ax.get_yaxis().tick_left()  
#xticks(fontsize=14)
plt.tick_params(axis="both", which="both", bottom="off", top="off",  
                labelbottom="on", left="off", right="off", labelleft="on")  
plt.savefig(outdir+"RER.png")

#plt.show()





# In[4]:
#Plots for the transilien, same cr... as for the RER, just the lines name have changed
fig = plt.figure(figsize=(15,15))
ax = fig.gca()
for rank, line in enumerate(["H","J","K","L","N","P","R","U"]):
  plt.plot_date(dftime.loc[dftime["Ligne"]==line].values[:,0],dftime.loc[dftime["Ligne"]==line].values[:,2],
    color=tableau20[rank], linestyle="-", lw=1.5)
  y = dftime.loc[dftime["Ligne"]==line].values[-1,2]
  if (line == "P"): y= y-.5 #fine tuning for cosmetixxx
  if (line == "U") : y= y+0.5 #fine tuning for cosmetixx
  plt.text("12/5/2015",y,"Ligne "+line +" "+ str(round(Punctual[line]))[0:2] + "\%",color=tableau20[rank])

plt.title(r"Ponctualit\'{e} des transiliens",fontsize=20)
plt.ylabel(r"proportion de voyageurs ``\textit{\'{a} l'heure}'' (\%)",fontsize=18)
plt.xlabel("Date",fontsize=18)
#plt.legend(loc=2, fancybox=True, fontsize=16)
plt.text("01/05/2013",62,r"La ponctualit\'{e} moyenne 2012-2015 est donn\'{e}e \`{a} c\^{o}t\'{e} du nom de la ligne", fontsize=13,fontstyle='italic')
#This sentence should prevent a call from the SNCF lawyer ;)
plt.text("01/05/2013", 61.0, r"Donn\'{e}es t\'{e}l\'{e}charg\'{e}es en f\'{e}vrier 2016 sur \textit{SNCF open-data} https://data.sncf.com/.",fontsize=12)

#get nice axis
plt.ylim(60,100)
plt.xlim("12/01/2012","01/01/2016")
plt.grid()
ax.spines["top"].set_visible(False)  
ax.spines["bottom"].set_visible(False)  
ax.spines["right"].set_visible(False)  
ax.spines["left"].set_visible(False)  
# Ensure that the axis ticks only show up on the bottom and left of the plot.  
# Ticks on the right and top of the plot are generally unnecessary chartjunk.  
ax.get_xaxis().tick_bottom()  
ax.get_yaxis().tick_left()  
#xticks(fontsize=14)
plt.tick_params(axis="both", which="both", bottom="off", top="off",  
                labelbottom="on", left="off", right="off", labelleft="on")  
plt.savefig(outdir+"Transiliens.png")


# In[4]:



